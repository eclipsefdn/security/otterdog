Moved to https://github.com/eclipse-csi/otterdog
============================================================

The content of this repo has been moved to https://github.com/eclipse-csi/otterdog.

This repository is therefore obsolete for any new developments, please head over to the repo @ GitHub.
